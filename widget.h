#ifndef WIDGET_H
#define WIDGET_H

#include <QMainWindow>

namespace Ui {
class Widget;
}

class Widget : public QMainWindow
{
    Q_OBJECT

public:
    explicit Widget(QMainWindow *parent = 0);
    ~Widget();
private slots:
    void on_pushButton_clicked();
private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
