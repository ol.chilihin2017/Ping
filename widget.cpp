#include "widget.h"
#include "ui_widget.h"
#include "winsock2.h"
#include "iphlpapi.h"
#include "icmpapi.h"

Widget::Widget(QMainWindow *parent) :
    QMainWindow(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    // Производим валидацию вводимых данных IP-адреса
    QString ipRange = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])";

    QRegExp ipRegex ("^" + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange
                     + "\\." + ipRange + "$");

    QRegExpValidator *ipValidator = new QRegExpValidator(ipRegex, this);
    ui->lineEdit->setValidator(ipValidator);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    // Объявляем переменные
    HANDLE hIcmpFile;                       // Обработчик
    unsigned long ipaddr = INADDR_NONE;     // Адрес назначения
    DWORD dwRetVal = 0;                     // Количество ответов
    char SendData[32] = "Data Buffer";      // Буффер отсылаемых данных
    LPVOID ReplyBuffer = NULL;              // Буффер ответов
    DWORD ReplySize = 0;                    // Размер буффера ответов

    // Устанавливаем IP-адрес из поля lineEdit
    ipaddr = inet_addr(ui->lineEdit->text().toStdString().c_str());
    hIcmpFile = IcmpCreateFile();   // Создаём обработчик
    int n = ui->lineEdit_2->text().toInt();
    // Выделяем память под буффер ответов
    ReplySize = sizeof(ICMP_ECHO_REPLY) + sizeof(SendData);
    ReplyBuffer = (VOID*) malloc(ReplySize);
     QString strMessage = "";
    for(int i = 0; i< n ; i++){
        // Вызываем функцию ICMP эхо запроса
        dwRetVal = IcmpSendEcho(hIcmpFile, ipaddr, SendData, sizeof(SendData),
                    NULL, ReplyBuffer, ReplySize, 1000);

        // создаём строку, в которою запишем сообщения ответа


        if (dwRetVal != 0) {
            // Структура эхо ответа
            PICMP_ECHO_REPLY pEchoReply = (PICMP_ECHO_REPLY)ReplyBuffer;
            struct in_addr ReplyAddr;
            ReplyAddr.S_un.S_addr = pEchoReply->Address;

            strMessage += "Sent icmp message to " + ui->lineEdit->text() + "\n";
            if (dwRetVal > 1) {
                strMessage += "Received " + QString::number(dwRetVal) + " icmp message responses \n";
                strMessage += "Information from the first response: ";
            }
            else {
                strMessage += "Received " + QString::number(dwRetVal) + " icmp message response \n";
                strMessage += "Information from the first response: ";
            }
                strMessage += "Received from ";
                strMessage += inet_ntoa( ReplyAddr );
                strMessage += "\n";
                strMessage += "Status = " + pEchoReply->Status;
                strMessage += "Roundtrip time = " + QString::number(pEchoReply->RoundTripTime) + " milliseconds \n";
        } else {
            strMessage += "Call to IcmpSendEcho failed.\n";
            strMessage += "IcmpSendEcho returned error: ";
            strMessage += QString::number(GetLastError());
        }
}
    ui->textEdit->setText(strMessage);
    free(ReplyBuffer); // Освобождаем память
}



